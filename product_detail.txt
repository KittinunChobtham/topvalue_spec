{
    "product_id": integer,
    "product_sku": string,
    "name": string,
    "original_price": float,
    "images": [string (URL)],
    "description": string,
    "description_images": [string (URL)],
    "additional_information": string (URL),
    "brand_name": string,
    "promotion": {
        "promotion_id": integer,
        "name": string,
        "discount": integer,
        "sale_price": float,
        "sale_expire": time,
        "sale_total_stock": integer,
        "sale_current_stock": integer
    },
    "customize": [{
        "title": string,
        "type": string, //exam string, URL, hex
        "values": [{
            "description": string,
            "display": string
        }]
    }],
    "stock": [{
        "key": [string], // exam ["red", "M"]
        "qualtity": integer
    }],
    "relation_status": {
        "in_cart": bool,
        "wishlist": bool
    },
    "shipping_methods": [{
            "name": string,
            "base_cost": float,
            "quantity_for_free": integer
        }
    ],
    "reviews": [{
        "review_topic": string,
        "review_description": string,
        "review_advantages": string,
        "review_disadvantages": string,
        "suggestion_status": bool,
        "review_images": [string (URL)]
    }]
}